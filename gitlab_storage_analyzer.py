#!/usr/bin/env python

# Description: Analyze the storage consumption data in GitLab, and optionally delete storage data. Requires GitLab API access. 
# Author: Michael Friedrich <mfriedrich@gitlab.com>
# License: MIT, (c) 2022 GitLab B.V.

import json 
import os
import sys 
import datetime
from time import sleep 
from enum import Enum
import gitlab 
# TODO
#from prometheus_client.core import GaugeMetricFamily, REGISTRY
#from prometheus_client import start_http_server

def error(text):
    logger("ERROR", text)
    sys.exit(1)

def logger(prefix, text):
    print("{prefix}: {text}".format(prefix=prefix, text=text))

# inherit from str and Enum to implicitly allow string comparisons 
class AnalyzeType(str, Enum):
    job_artifacts = 'job_artifacts'
    repository = 'repository'
    container = 'container'
    packages = 'packages'

class DeleteMode(str, Enum):
    none = 'none'
    threshold = 'threshold'
    all = 'all'

class OutputType(str, Enum):
    markdown = 'markdown'
    json = 'json'  

class GitLabStorageSum(object):
    def __init__(self, verbose, gl_server,
        gl_group_id, gl_project_id, gl_token, gl_output_type, gl_output_file, gl_analyze_type,
        gl_delete_mode, gl_job_artifact_threshold_size, gl_job_artifact_threshold_age, gl_exclude_file_type):
        # params
        self.verbose = verbose
        self.output_type = gl_output_type
        self.output_file = gl_output_file
        self.gl_analyze_type = gl_analyze_type
        self.gl_server = gl_server
        self.gl_group_id = gl_group_id
        self.gl_project_id = gl_project_id
        self.gl_token = gl_token
        self.gl_job_artifact_threshold_size = float(gl_job_artifact_threshold_size)
        self.gl_job_artifact_threshold_age = float(gl_job_artifact_threshold_age)
        self.gl_delete_mode = gl_delete_mode
        self.gl_exclude_file_type = gl_exclude_file_type
        # data 
        self.projects = []
        self.stats_data = []
        self.job_data = []
        self.repository_data = []
        self.jobs_marked_delete_artifacts = []
        self.group_projects = []

    def __str__(self):
        # Print class attributes but hide the token when debugging.
        d = self.__dict__
        return str(self.__class__) + ": " + str({x: d[x] for x in d if "token" not in x})

    def log_debug(self, text):
        if self.verbose:
            print("DEBUG: {d}".format(d=text))        

    def connect(self):
        self.log_debug("Connecting to GitLab API at {s}".format(s=self.gl_server))
        # Supports personal/project/group access token
        # https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens 
        self.gl = gitlab.Gitlab(self.gl_server, private_token=self.gl_token)

    def get_projects(self):
        if self.gl_project_id:
            self.log_debug("Using project_id {id} to collect projects".format(id=self.gl_project_id))

            self.projects.append(self.gl.projects.get(self.gl_project_id))
        else:
            # Use the group id 
            self.log_debug("Using group_id {id} to collect projects".format(id=self.gl_group_id))

            group = self.gl.groups.get(self.gl_group_id)
            # Important: Large result set requires iterator and pagination
            # https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination             
            projects = group.projects.list(include_subgroups=True, all=True, pagination="keyset", order_by="id", per_page=100, iterator=True)

            for project in projects: 
                # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples 
                # GroupProject and SharedProject objects returned by these two API calls are very limited, and do not provide all the features of Project objects. 
                # If you need to manipulate projects, create a new Project object:
                manageable_project = self.gl.projects.get(project.id)
                
                self.projects.append(manageable_project)


    def request_job_artifact_data(self):
        for project in self.projects:
            self.request_project_job_artifacts_data(project)

    def request_project_job_artifacts_data(self, project):
        # Important: Large result set requires iterator and pagination
        # https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination 
        jobs = project.jobs.list(pagination="keyset", order_by="id", per_page=100, iterator=True)

        # https://docs.gitlab.com/ee/api/jobs.html#list-project-jobs 
        for job in jobs:
            # Skip jobs without artifacts 
            artifacts = job.attributes['artifacts']
            artifacts = list(filter(lambda x: x['file_type'] not in self.gl_exclude_file_type, artifacts))
            if not artifacts:
                continue 

            # job age, need to parse API format: 2022-08-10T20:41:08.270Z
            created_at = datetime.datetime.strptime(job.created_at, '%Y-%m-%dT%H:%M:%S.%fZ')
            now = datetime.datetime.now()
            age = (now - created_at).total_seconds() 
            
            #self.log_debug(age)

            # 'artifacts': [{'file_type': 'trace', 'size': 2697, 'filename': 'job.log', 'file_format': None}] 
            for a in artifacts:
                data = {
                    "project_id": project.id,
                    "project_web_url": project.web_url,
                    "project_path_with_namespace": project.path_with_namespace,
                    "job_id": job.id,
                    "age": age,
                    "artifact_filename": a['filename'],
                    "artifact_file_type": a['file_type'],
                    "artifact_size": a['size']
                }

                self.job_data.append(data)

                # mark jobs for deletion if threshold matches
                if self.gl_delete_mode == DeleteMode.threshold:
                    if self.eval_threshold_size(a['size']) or self.eval_threshold_age(age):
                        # mark the job for deletion. Store all attributes for easier navigation; at least project_id and job_id are required for later lookups by python-gitlab.
                        self.jobs_marked_delete_artifacts.append(data)

        self.log_debug(self.job_data)   

    def gen_summary_job_artifacts(self):
        self.request_job_artifact_data() # TODO: consider caching

        if self.output_type == OutputType.json:
            out = self.render_json_output(self.job_data)

            return out 

        if self.output_type == OutputType.markdown:
            fields = ['Project Name', 'Project ID', 'Job ID', 'File', 'Type', 'Size (MB)', 'Age']

            # Markdown needs table rendering; try a generic format later. 
            out = ""
            out += "| " + " | ".join(fields) + " |\n"
            out += "| " + " | ".join(['-' * len(x) for x in fields]) + " |\n" # preserve the column length from fields 

            size_sum = 0.0
            size_save_sum = 0.0
            age_save_sum = 0.0

            for c in self.job_data:

                size_raw = c['artifact_size']
                size_str = self.render_size_mb(size_raw)
                age_raw = c['age']
                age_str = self.render_age_time(age_raw)

                # TODO: Only archive artifacts can be deleted and save storage. https://gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer/-/issues/14 
                if c['artifact_file_type'] and "archive" in c['artifact_file_type']:
                    # Threshold: size
                    # if threshold is set, add this detail when matched
                    if self.eval_threshold_size(size_str):
                        size_str = "🏗️ **{size} (> {thr} MB)**".format(size=size_str, thr=self.gl_job_artifact_threshold_size)
                        size_save_sum += size_raw

                    # Threshold: age 
                    if self.eval_threshold_age(age_raw):
                        age_str = "⏰ **{age} ({age_raw} > {thr} sec)**".format(
                            age=age_str, age_raw=age_raw, thr=self.gl_job_artifact_threshold_age)
                        age_save_sum += size_raw
                    else:
                        age_str = "{age} ({age_raw} sec)".format(age=age_str, age_raw=age_raw) # readable dateformat by default
                else:
                    age_str = "{age} ({age_raw} sec)".format(age=age_str, age_raw=age_raw) # readable dateformat by default

                # TODO: Group by artifact_file_type 

                # Render table line 

                # Project name with URL
                project_url = c['project_web_url']
                storage_url = "{u}/-/usage_quotas".format(u=project_url)
                job_url = "{u}/-/jobs/{job_id}".format(u=project_url, job_id=c['job_id'])

                p_name = "[{name}]({u_w}) ([Usage quota]({u_uq}))".format(
                    name=c['project_path_with_namespace'], u_w=project_url, u_uq=storage_url)
                job_id = "[{job_id}]({u_w})".format(job_id=c['job_id'], u_w=job_url)

                # add table line 
                out += "| {p_name} | {p_id} | {j_id} | {f} | {t} | {s} | {a} |\n".format(
                    p_name=p_name, p_id=c['project_id'], j_id=job_id, f=c['artifact_filename'], 
                    t=c['artifact_file_type'], s=size_str, a=age_str) 

                # sum 
                size_sum += size_raw    
               
            out += """\n\n### Results Summary\n\n
_All projects combined._

_Note that job artifact archives can be deleted, other artifact types are not yet supported for deletion (trace logs, metadata, etc.)
This can result in the same size reports no matter how often the script is run. Follow https://gitlab.com/gitlab-org/gitlab/-/issues/373917 for analysis in GitLab._

| Description | Size (MB) | 
|-------------|-----------|            
"""
            
            out += "| Job artifacts size | {sum} MB |\n".format(sum=self.render_size_mb(size_sum))

            if self.gl_job_artifact_threshold_size > 0:
                out += "| Size threshold ({t} MB) Cleanup Potential | {save} MB |\n".format(
                    t=self.gl_job_artifact_threshold_size, save=self.render_size_mb(size_save_sum))

            if self.gl_job_artifact_threshold_age > 0:
                out += "| Age threshold ({t} sec) Cleanup Potential | {save} MB |\n".format(
                    t=self.gl_job_artifact_threshold_age, save=self.render_size_mb(age_save_sum))

            out += "\n\n"

            return out 

    # Stats 
    def request_stats_data(self):
        for project in self.projects:
            self.request_project_stats_data(project)

    def request_project_stats_data(self, project):
        p_with_stats = self.gl.projects.get(project.id, statistics=True) # enable statistics 

        self.stats_data.append({
            'project_id': project.id,
            "project_web_url": project.web_url,
            "project_path_with_namespace": project.path_with_namespace,            
            'stats_storage': p_with_stats.statistics['storage_size'],
            'stats_repository': p_with_stats.statistics['repository_size'],
            'stats_job_artifacts': p_with_stats.statistics['job_artifacts_size'],
            'stats_packages': p_with_stats.statistics['packages_size'],
            'stats_wiki': p_with_stats.statistics['wiki_size'],
            'stats_snippets': p_with_stats.statistics['snippets_size'],
            'stats_uploads': p_with_stats.statistics['uploads_size']
        })


    def gen_summary_stats(self):
        self.request_stats_data() # TODO: caching 

        if self.output_type == OutputType.json:
            out = self.render_json_output(self.stats_data)

            return out 

        if self.output_type == OutputType.markdown:
            fields = ['Project Name', 'Project ID', 'Storage (MB)', 'Repository (MB)', 'Job Artifacts (MB)', 'Packages (MB)', 'Wiki (MB)', 'Snippets (MB)', 'Uploads (MB)' ]

            # Markdown needs table rendering; try a generic format later. 
            out = ""
            out += "| " + " | ".join(fields) + " |\n"
            out += "| " + " | ".join(['-' * len(x) for x in fields]) + " |\n" # preserve the column length from fields 

            for c in self.stats_data:
                # Project name with URL
                project_url = c['project_web_url']
                storage_url = "{u}/-/usage_quotas".format(u=project_url)

                p_name = "[{name}]({u_w}) ([Usage quota]({u_uq}))".format(
                    name=c['project_path_with_namespace'], u_w=project_url, u_uq=storage_url)

                # add table line 
                out += "| {p_name} | {p_id} | {storage} | {repository} | {job_artifacts} | {packages} | {wiki} | {snippets} | {uploads} |\n".format(
                    p_name=p_name, p_id=c['project_id'], 
                    storage=self.render_size_mb(c['stats_storage']),
                    repository=self.render_size_mb(c['stats_repository']),
                    job_artifacts=self.render_size_mb(c['stats_job_artifacts']),
                    packages=self.render_size_mb(c['stats_packages']),
                    wiki=self.render_size_mb(c['stats_wiki']), 
                    snippets=self.render_size_mb(c['stats_snippets']),
                    uploads=self.render_size_mb(c['stats_uploads'])
                    )

            return out 



    # MAIN entry point
    def prepare_req(self):
        # required
        self.connect()
        self.get_projects() # do this once and store projects for all operations

    # MAIN entry point
    def create_summary(self):
        out = """# GitLab Storage Analyzer Report
        
_Generated on {now}_

## Project Statistics

""".format(now=datetime.datetime.now())

        out += self.gen_summary_stats()

        if self.gl_analyze_type == AnalyzeType.job_artifacts:
            out += """

## Pipeline Job Artifacts Summary 

"""

            # Job artifacts 
            out += self.gen_summary_job_artifacts()

        if self.gl_analyze_type == AnalyzeType.repository:
            out += """

## Repository Summary 

"""
            #out += self.gen_summary_repository()
            
        # Additional storage types. TODO: https://gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer/-/issues/2 


        #out += """
        ### Container Registry Summary 
        #"""

        # stdout on terminal 
        print(out)

        # write summary to report file
        self.write_output_file(out)        

    # MAIN entry point
    def run_cleanup(self):
        if self.gl_delete_mode == DeleteMode.none:
            self.log_debug("Delete mode not enabled, bailing out.")
            return # safety 

        # erase all jobs with artifacts and traces for all projects 
        if self.gl_delete_mode == DeleteMode.all:
            for project in self.projects:
                print("Deleting job artifacts for project {id}".format(id=project.id))

                # Important: Large result set requires iterator and pagination
                # https://python-gitlab.readthedocs.io/en/stable/api-usage.html#pagination 
                jobs = project.jobs.list(pagination="keyset", order_by="id", per_page=100, iterator=True)

                for job in jobs:
                    self.log_debug("Deleting artifacts for job {id}".format(id=job.id))
                    job.delete_artifacts() # erase() is too invasive and may block. 

        # threshold deletion only deletes matched job artifacts 
        if self.gl_delete_mode == DeleteMode.threshold:
            for d in self.jobs_marked_delete_artifacts:

                project = self.gl.projects.get(d['project_id'], lazy=True)
                job = project.jobs.get(d['job_id'], lazy=True)

                # delete artifacts. erase() is too invasive. 
                print("Deleting artifacts for job {id} with size {s}".format(id=job.id, s=d['artifact_size']))
                job.delete_artifacts()

        print("Deletion successful. Please run the script again to fetch a new summary.\n")

    def eval_threshold_size(self, v):
        return self.gl_job_artifact_threshold_size > 0 and (float(v) > float(self.gl_job_artifact_threshold_size))

    def eval_threshold_age(self, v):
        return self.gl_job_artifact_threshold_age > 0 and (float(v) > float(self.gl_job_artifact_threshold_age))

    def write_output_file(self, text):
        with open(self.output_file, "w") as f: # TODO: exceptions  
            f.write(text)

    def render_size_mb(self, v):
        return "%.4f" % (v / 1024 / 1024)

    def render_age_time(self, v):
        return str(datetime.timedelta(seconds = v))

    def render_json_output(self, data):
        return json.dumps(data, indent=4, sort_keys=True)

    def render_prom_metric(self):
        # TODO 
        pass         

    def collect(self):
        # Prometheus metrics 
        # TODO 
        pass


## main 
if __name__ == '__main__':
    gl_verbose = os.environ.get('GL_VERBOSE', False)
    gl_server = os.environ.get('GL_SERVER', 'https://gitlab.com')

    gl_group_id = os.environ.get('GL_GROUP_ID')
    gl_project_id = os.environ.get('GL_PROJECT_ID')  

    if not gl_group_id and not gl_project_id:
        error("Please specify the GL_GROUP_ID or GL_PROJECT_ID env variable")

    if gl_group_id and gl_project_id:
        logger("INFO", "Using GL_PROJECT_ID {p_id} with preference over GL_GROUP_ID {g_id}".format(p_id=gl_project_id, g_id=gl_group_id))   

    gl_token = os.environ.get('GL_TOKEN')

    if not gl_token:
        error("Please specifiy the GL_TOKEN env variable")

    gl_output_type = os.environ.get('GL_OUTPUT_TYPE', OutputType.markdown)
    gl_output_file = os.environ.get('GL_OUTPUT_FILE', 'gitlab_storage_analysis_report.md')

    gl_analyze_type = os.environ.get('GL_ANALYZE_TYPE', AnalyzeType.job_artifacts)

    gl_job_artifact_threshold_size = os.environ.get('GL_THRESHOLD_SIZE_MB', -1)
    gl_job_artifact_threshold_age = os.environ.get('GL_THRESHOLD_AGE_SEC', -1)      

    gl_delete_mode = os.environ.get('GL_DELETE_MODE', DeleteMode.none)

    if not gl_delete_mode or gl_delete_mode == DeleteMode.none:
        gl_delete_mode = DeleteMode.none
    elif gl_delete_mode == DeleteMode.threshold:
        if not gl_job_artifact_threshold_size and not gl_job_artifact_threshold_age:
            error("Please define GL_THRESHOLD_SIZE_MB or GL_THRESHOLD_AGE_SEC to enable threshold based deletions.")
    elif gl_delete_mode == DeleteMode.all:
        print("Warning: Going to delete all job artifacts for selected projects/groups. Sleeping for 10s, press ctrl+c to abort.")
        for i in range(10):
            print(".")
            sleep(1)
    else:
        error("Wrong value for GL_DELETE_MODE: {d}. Can be: 'none' (default), 'threshold', 'all'".format(d=gl_delete_mode))

    gl_exclude_file_type = os.environ.get('GL_EXCLUDE_FILE_TYPE', '').split(',')

    # Instantiate object 
    gl_storage_analyzer = GitLabStorageSum(gl_verbose, gl_server, 
        gl_group_id, gl_project_id, gl_token, gl_output_type, gl_output_file, gl_analyze_type,
        gl_delete_mode, gl_job_artifact_threshold_size, gl_job_artifact_threshold_age, gl_exclude_file_type)

    # required
    gl_storage_analyzer.prepare_req()

    # read only 
    gl_storage_analyzer.create_summary()

    # write 
    gl_storage_analyzer.run_cleanup()


