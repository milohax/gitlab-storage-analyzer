<!-- Hey, thanks for contributing! :-)

Please fill in the template below, and consider looking into a merge request adding changes too.
-->

## Problem to solve

<!-- Briefly describe the problem and use case, add examples with URLs and screenshots. Add steps to reproduce when describing a bug.-->

1.
1.
1.

## Proposal

<!-- Describe the proposed changes/addition, possible implementation details, and workflows to reproduce. -->

- [ ] 
- [ ] 
- [ ] 

## How does success look like

<!-- Expected results, better usage, etc. -->

## Resources

<!-- Add Merge Requests, documentation URLs, etc. -->


<!-- Quick actions

/label ~bug
/label ~enhancement

Select the storage type from https://gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer/-/labels 
/label ~"storage-type::"

-->
/label ~enhancement