# GitLab Storage Analyzer

> **Note**: This MIT-licensed script is not officially supported by GitLab and comes without warranty.
>
> By default, only read operations are performed and no data is deleted. Use at your own risk.

Project to help analyze the storage consumption data, and optionally delete storage data.

## Features

### General Features

- Query a project, or a group with projects (sub groups supported)
- Generate summary report output and file
  - Markdown table summary with names and URLs
  - Results summary with cleanup potential

### Job Artifacts 

- Storage type summaries
- Optional thresholds
  - Size (MB)
  - Age (seconds) similar to [expire_in](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#when-job-artifacts-are-deleted) 
- Highlight thresholds in summary and show estimated savings for cleanup.
- Filter job artifacts to delete matching the threshold
- Cleanup deletion types: threshold or all

![Markdown table rendered for Job Artifacts](docs/images/report_job_artifacts.png)

### More storage types

Please follow the [issues](https://gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer/-/issues), and/or open an issue or merge request. Thanks! 


## Requirements

- GitLab API access with a [personal/project/group access token](https://docs.gitlab.com/ee/api/index.html#personalprojectgroup-access-tokens)
  - Permission scopes: `read_api` for analysis, `api` for analysis and deletion/cleanup 

### Container

Docker, Rancher Desktop, podman or any other container runtime.

### Source

- Python 3.9+
- Libraries: [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/) (source install, &gt;= 3.9.0)

macOS with Homebrew:

```shell
brew install python3
pip3 install -r requirements.txt
```

## Usage

Export the required configuration options as environment variables on the terminal, or in container environments.

### Source

```
export GL_TOKEN=XXX
export GL_PROJECT_ID=38485106

python3 gitlab_storage_analyzer.py
```

### Container

```shell
docker pull registry.gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer:latest

docker run -ti -v "`pwd`/reports:/tmp" \
 -e "GL_TOKEN=$GITLAB_TOKEN" \
 -e "GL_PROJECT_ID=38485106" \
 -e "GL_OUTPUT_FILE=/tmp/gitlab_storage_analyzer_report.md" \
  registry.gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer:latest \
 python /app/gitlab_storage_analyzer.py
```


### Configuration

| Variable | Value | Description |
|----------|-------|-------------|
| GL_TOKEN | . | Personal Auth Token with permission to read the GitLab API. Optional write permissions for deletion needed |
| GL_PROJECT_ID | 38485106 | Project ID to analyse. Superseds GL_GROUP_ID if set. |
| GL_GROUP_ID | 8034603 | Group ID to analyse, will navigate into all sub groups and projects. |
| GL_ANALYZE_TYPE | `job_artifacts` (default) | Filter by specific storage type to analyze. | 
| GL_THRESHOLD_SIZE_MB | 10 | Size threshold in MB. Will be shown in Markdown tables, and used for the delete mode `threshold` |
| GL_THRESHOLD_AGE_SEC | 86400 | Age threshold in seconds. Will be shown in Markdown tables, and used for the delete mode `threshold` | 
| GL_DELETE_MODE | `none` (default), `threshold`, `all`  | Optional deletion mode after analysis. |
| GL_OUTPUT_TYPE | `markdown` (default) | Output type. `json` is used for debugging internally. |
| GL_OUTPUT_FILE | `gitlab_storage_analyzer_report.md` (default) | Path to the file where the report is written to. | 
| GL_VERBOSE | 0 or 1 | Enable verbose debug logging. |
| GL_EXCLUDE_FILE_TYPE | `none` (default) | Exclude one or more file types. Separate multiple values by commas, e.g. `trace,archive,metadata`.         |

### Script does not work?

There are community provided alternatives with scripts and resources:

- https://gitlab.com/thelabnyc/gitlab-storage-cleanup
- https://gitlab.com/JonathonReinhart/gitlab-artifact-cleanup 
- https://forum.gitlab.com/t/remove-all-artifact-no-expire-options/9274 
- https://mdleom.com/blog/2022/08/09/remove-gitlab-artifacts/ 
- https://stackoverflow.com/questions/71513286/clean-up-history-in-gitlab-pipeline
- https://sandstorm.de/de/blog/post/cleaning-up-gitlab-build-artifacts.html 

Please ensure to review these before running any scripts or commands that could delete too much data. 

#### Alternative: Raw API Calls

The script wraps the GitLab API calls to fetch project statistics, and iterates through the list of pipeline jobs for example. You can replicate these calls by using raw API calls. 

Fetch project statistics: https://docs.gitlab.com/ee/api/projects.html#get-single-project 

```
curl -s -H "Authorization: Bearer $GITLAB_TOKEN" 'https://gitlab.com/api/v4/projects/37113500?statistics=true' | jq -c '.statistics' | jq
{
  "commit_count": 50,
  "storage_size": 39477528,
  "repository_size": 7214202,
  "wiki_size": 0,
  "lfs_objects_size": 0,
  "job_artifacts_size": 32152749,
  "pipeline_artifacts_size": 0,
  "packages_size": 0,
  "snippets_size": 0,
  "uploads_size": 110577
}
```


Delete project job artifacts: https://docs.gitlab.com/ee/api/job_artifacts.html#delete-project-artifacts

```
curl -s -H "Authorization: Bearer $GITLAB_TOKEN" -X DELETE 'https://gitlab.com/api/v4/projects/37113500/artifacts'
```

### Demo 

Open a terminal and prepare the environment variables noted in the steps below. 

0. Prepare your GitLab Personal Auth Token with `export GL_TOKEN=xxx`
1. Create a new project on GitLab.com 
2. Copy the project ID from the project overview, export with `export GL_PROJECT_ID=xxx`
3. Navigate to `CI/CD > Editor` and create a new pipeline that includes the job artifact generator config.

```yaml
include:
    - remote: https://gitlab.com/gitlab-da/use-cases/efficiency/job-artifact-generator/-/raw/main/.gitlab-ci.yml
```

4. Run the pipeline again in `CI/CD > Pipelines > Run Pipeline`
5. Execute the script to analyse the job artifacts usage. Specify the deletion limit to `10MB` size and `300s` age.

```shell
docker run -ti -v "`pwd`/reports:/tmp" \
 -e "GL_TOKEN=$GITLAB_TOKEN" \
 -e "GL_PROJECT_ID=38485106" \
 -e "GL_OUTPUT_FILE=/tmp/gitlab_storage_analyzer_report.md" \
 registry.gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer:latest \
 python /app/gitlab_storage_analyzer.py
```

6. Repeat the command, but enable the threshold based deletion for all matches. 

```shell
docker run -ti -v "`pwd`/reports:/tmp" \
 -e "GL_TOKEN=$GITLAB_TOKEN" \
 -e "GL_PROJECT_ID=38485106" \
 -e "GL_THRESHOLD_SIZE_MB=10" \
 -e "GL_THRESHOLD_AGE_SEC=300" \
 -e "GL_DELETE_MODE=threshold" \
 -e "GL_OUTPUT_FILE=/tmp/gitlab_storage_analyzer_report.md" \
 registry.gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer:latest \
 python /app/gitlab_storage_analyzer.py
```

The same steps work with creating a new subgroup and 2+ projects that include the job artifact generator config. An example group can be found in https://gitlab.com/gitlab-da/use-cases/efficiency/job-artifact-generator 

```shell
docker run -ti -v "`pwd`/reports:/tmp" \
 -e "GL_TOKEN=$GITLAB_TOKEN" \
 -e "GL_GROUP_ID=56595735" \
 -e "GL_OUTPUT_FILE=/tmp/gitlab_storage_analyzer_report.md" \
 registry.gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer:latest \
 python /app/gitlab_storage_analyzer.py
```

### Usage Examples

Example with project:

```
export GL_TOKEN=$GITLAB_TOKEN
export GL_GROUP_ID=8034603
export GL_PROJECT_ID=38485106

export GL_VERBOSE=1

python3 gitlab_storage_analyzer.py
```

Example with size thresholds:

```
export GL_TOKEN=$GITLAB_TOKEN
export GL_PROJECT_ID=38485106

export GL_THRESHOLD_SIZE_MB=10

python3 gitlab_storage_analyzer.py
```

Example with age thresholds:

```
export GL_TOKEN=$GITLAB_TOKEN
export GL_PROJECT_ID=38485106

export GL_THRESHOLD_AGE_SEC=86400

python3 gitlab_storage_analyzer.py
```

Example with size thresholds and deletion.

```
export GL_TOKEN=$GITLAB_TOKEN
export GL_PROJECT_ID=38485106

export GL_THRESHOLD_SIZE_MB=10
export GL_DELETE_MODE='threshold'

python3 gitlab_storage_analyzer.py
```

Example with deleting all job data:

```
export GL_TOKEN=$GITLAB_TOKEN
export GL_PROJECT_ID=38485106

export GL_DELETE_MODE='all'

python3 gitlab_storage_analyzer.py
```

## Contributing

Thanks for considering to contribute :-)

### Recommendations

The script is written in Python 3, basic knowledge is recommended to read and modify the source code.

The [python-gitlab](https://python-gitlab.readthedocs.io/en/stable/) library helps to abstract the GitLab API. You'll need to understand its objects, methods and attributes. 

You can use VS Code with Python Intellisense, or any other editor that supports syntax highlighting and suggestions. 

### Test data

Include the [Job Artifact Generator](https://gitlab.com/gitlab-da/use-cases/efficiency/job-artifact-generator) and use the newly created project id in tests. For group tests, create multiple projects in a group that generate artifact data.

```yaml
include:
    - remote: https://gitlab.com/gitlab-da/use-cases/efficiency/job-artifact-generator/-/raw/main/.gitlab-ci.yml

generator:
    parallel:
        matrix:
            - MB_COUNT: [1, 5, 10, 20, 50]

```

### Development Environments

#### Gitpod

TODO

#### Local


```
pip3 install -r requirements_dev.txt

pyflakes .
```

### Releases

Create a new tag and sign it with your GPG key.

```
git tag v0.1.0 -m "Release v0.1.0" -s
```


[Create a new release in GitLab](https://gitlab.com/gitlab-da/use-cases/gitlab-api/gitlab-storage-analyzer/-/releases/new):

- Link the tag name
- Same name for the release
- Link the milestone.
- Copy the README features into the description
